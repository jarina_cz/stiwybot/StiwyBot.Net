# StiwyBot.NET

A Simple Disord Bot written in C# and .NET Core 2 using a Discord.NET library.

## Installation

```bash
git clone https://github.com/stefanjarina/StiwyBot.NET.git
cd StiwyBot.NET/StiwyBot.NET
```

Get token from Discord Developers page and then create environment variable

```bash
# Linux
export DISCORD_BOT_TOKEN_STIWY_BOT="your_token"
# Windows PowerShell
$Env:DISCORD_BOT_TOKEN_STIWY_BOT="your_token"
```

Edit `config.json` with your favourite text editor.

Fetch all the dependencies

```bash
dotnet restore
```

## Run

### In Development

```bash
dotnet watch run
```

## Tests
- Not yet implemented

## LICENSE

MIT
