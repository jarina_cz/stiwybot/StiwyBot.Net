﻿using System.IO;
using Newtonsoft.Json;

namespace StiwyBot.NET
{
    public static class Config
    {
        private const string ConfigFile = "config.json";
        public static BotConfig Bot;

        static Config()
        {

            if (!File.Exists(ConfigFile))
            {
                Bot = new BotConfig();
                string json = JsonConvert.SerializeObject(Bot);
                File.WriteAllText(ConfigFile, json);
            }
            else
            {
                string json = File.ReadAllText(ConfigFile);
                Bot = JsonConvert.DeserializeObject<BotConfig>(json);
            }
        }

        public struct BotConfig
        {
            public string CmdPrefix;
            public bool ProfanityFilter;
            public string ProfanityFilterString;
        }
    }
}
