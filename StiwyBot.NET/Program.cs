﻿using Discord;
using Discord.WebSocket;
using StiwyBot.NET.events;
using System;
using System.Threading.Tasks;

namespace StiwyBot.NET
{
    class Program
    {
        private DiscordSocketClient _client;
        private MessageHandler _handler;

        static void Main()
        {
            new Program().StartAsync().GetAwaiter().GetResult();
        }

        private async Task StartAsync()
        {
            string token = Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN_STIWY_BOT");
            if (token == null)
            {
                Console.WriteLine("You must specify 'DISCORD_BOT_TOKEN_STIWY_BOT' environment variable");
                return;
            }
            if (Config.Bot.CmdPrefix == null)
            {
                Console.WriteLine("Please update your configuration file");
                return;
            }

            // Events subscription
            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Warning
            });
            _client.Log += Log;
            _client.Connected += ConnectLog;
            _client.Ready += ReadyLog;
            _client.ChannelCreated += ChannelEvents.OnChannelCreated;
            _client.ChannelDestroyed += ChannelEvents.OnChannelDeleted;
            _client.UserJoined += GuildEvents.OnUserJoined;
            _client.RoleCreated += RoleEvents.OnRoleCreated;
            _client.RoleDeleted += RoleEvents.OnRoleDeleted;

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            Global.Client = _client;

            // Message handler
            _handler = new MessageHandler(_client);

            await Task.Delay(-1);
        }

        private static async Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.Message);
            await Task.CompletedTask;
        }

        private static async Task ConnectLog()
        {
            Console.WriteLine("Your Bot connected successfuly to Discord.");
            await Task.CompletedTask;
        }

        private static async Task ReadyLog()
        {
            Console.WriteLine("Bot now listens for commands and events...");
            await Task.CompletedTask;
        }
    }
}