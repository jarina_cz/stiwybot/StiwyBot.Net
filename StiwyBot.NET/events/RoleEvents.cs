﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace StiwyBot.NET.events
{
    static class RoleEvents
    {
        public static async Task OnRoleCreated(SocketRole role)
        {
            Console.WriteLine($"A new role '{role.Name}' has been created");
            await Task.CompletedTask;
        }

        public static async Task OnRoleDeleted(SocketRole role)
        {
            Console.WriteLine($"A role '{role.Name}' has been deleted");
            await Task.CompletedTask;
        }
    }
}
