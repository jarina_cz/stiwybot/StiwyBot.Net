﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace StiwyBot.NET.events
{
    static class ChannelEvents
    {
        public static async Task OnChannelCreated(SocketChannel ch)
        {
            if (ch is SocketDMChannel) return;
            Console.WriteLine($"A channel of type '{ch.GetType().Name}' with name '{ch}' was created");
            if (ch is SocketTextChannel)
            {
                var channel = ch as SocketTextChannel;
                await channel.SendMessageAsync("Welcome to your new channel");
            }
            await Task.CompletedTask;
        }

        public static async Task OnChannelDeleted(SocketChannel ch)
        {
            Console.WriteLine($"A channel '{ch}' was deleted");
            await Task.CompletedTask;
        }
    }
}
