﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace StiwyBot.NET.events
{
    static class GuildEvents
    {
        public static async Task OnUserJoined(SocketGuildUser u)
        {
            Console.WriteLine($"User {u.Nickname} joined {u.Guild.Name}");
            await u.Guild.DefaultChannel.SendMessageAsync($"Please welcome {u.Nickname} to the server");
            await Task.CompletedTask;
        }
    }
}
