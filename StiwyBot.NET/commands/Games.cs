﻿using Discord;
using System;
using System.Threading.Tasks;
using Discord.Commands;
using System.Linq;

namespace StiwyBot.NET.commands
{
    public class Games : ModuleBase<SocketCommandContext>
    {

        [Command("8ball")]
        public async Task Ball([Remainder]string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                await Context.Channel.SendMessageAsync("You need to ask a question");
            }
            string[] possibleAnswers = {
                "It is certain",
                "It is decidedly so",
                "Without a doubt",
                "Yes definitely",
                "You may rely on it",
                "As I see it, yes",
                "Most likely",
                "Outlook good",
                "Yes.",
                "Signs point to yes",
                "Reply hazy, try again",
                "Ask again later",
                "Better not tell you now",
                "Cannot predict right now",
                "Concentrate and ask again",
                "Don\"t count on it",
                "My reply is no",
                "My sources say no",
                "Outlook not so good",
                "Very doubtful"
            };
            Random rand = new Random();
            var answer = possibleAnswers[rand.Next(0, possibleAnswers.Length)];
            await Context.Channel.SendMessageAsync($"Magic ball says: {answer}.");
        }

        [Command("love")]
        public async Task Love([Remainder]string args = "")
        {
            var user = Context.Message.MentionedUsers.First();
            if (user == null)
                await Context.Channel.SendMessageAsync("You need to mention someone.");
            var rand = new Random();
            await Context.Channel.SendMessageAsync($"There is {rand.Next(0, 101)}% love between {Context.Message.Author.Username} and {user?.Username}");
        }
    }
}
