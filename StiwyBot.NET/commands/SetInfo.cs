﻿using System;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

namespace StiwyBot.NET.commands
{
    public class SetInfo : ModuleBase<SocketCommandContext>
    {
        [Command("setgame")]
        public async Task SetGame([Remainder]string message)
        {
            var game = message.Trim();
            await Context.Client.SetGameAsync(game);
        }

        [Command("setstatus")]
        public async Task SetStatus(string status)
        {
            Console.WriteLine(status);
            Hashtable statuses = new Hashtable
            {
                ["online"] = Discord.UserStatus.Online,
                ["offline"] = Discord.UserStatus.Offline,
                ["afk"] = Discord.UserStatus.AFK,
                ["dnd"] = Discord.UserStatus.DoNotDisturb,
                ["idle"] = Discord.UserStatus.Idle,
                ["invisible"] = Discord.UserStatus.Invisible
            };

            Discord.UserStatus newStatus = Discord.UserStatus.Online;
            if (statuses.ContainsKey(status))
            {
                newStatus = (Discord.UserStatus)statuses[status];
            }
            Console.WriteLine(newStatus);

            await Context.Client.SetStatusAsync(newStatus);
        }
    }
}
