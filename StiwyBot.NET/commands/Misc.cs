using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace StiwyBot.NET.commands
{
    public class Misc : ModuleBase<SocketCommandContext>
    {
        [Command("ping")]
        public async Task Ping()
        {
            await Context.Channel.SendMessageAsync($"Pong! '{Math.Round((DateTime.Now - Context.Message.CreatedAt).TotalMilliseconds)}ms'");
        }

        [Command("echo")]
        public async Task Echo([Remainder] string message)
        {
            var embedBuilder = new EmbedBuilder();
            embedBuilder.WithTitle("Message by " + Context.User.Username);
            embedBuilder.WithDescription(message);
            embedBuilder.WithColor(new Color(0, 255, 0));

            var embed = embedBuilder.Build();

            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("pick")]
        public async Task Pick([Remainder] string message)
        {
            string[] options = message.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            Random r = new Random();
            string selection = options[r.Next(0, options.Length)];

            var embedBuilder = new EmbedBuilder();
            embedBuilder.WithTitle("Choice for " + Context.User.Username);
            embedBuilder.WithDescription(selection);
            embedBuilder.WithColor(new Color(255, 255, 0));
            embedBuilder.WithThumbnailUrl(Context.User.GetAvatarUrl());

            var embed = embedBuilder.Build();

            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("person")]
        public async Task GetRandomPerson()
        {
            string json = "";
            using (WebClient client = new WebClient())
            {
                json = client.DownloadString("https://randomuser.me/api/?gender=female&nat=US");
            }

            var dataObject = JsonConvert.DeserializeObject<dynamic>(json);

            string firstName = dataObject.results[0].name.first.ToString();
            string lastName = dataObject.results[0].name.last.ToString();
            string avatarUrl = dataObject.results[0].picture.large.ToString();

            var embedBuilder = new EmbedBuilder();
            embedBuilder.WithThumbnailUrl(avatarUrl);
            embedBuilder.WithTitle("Generated Person");
            embedBuilder.AddField("First Name", firstName, true);
            embedBuilder.AddField("Last Name", lastName, true);

            var embed = embedBuilder.Build();

            await Context.Channel.SendMessageAsync("", false, embed);
        }

        private static bool UserIsSecretOwner(SocketGuildUser user)
        {
            const string targetRoleName = "Secret Owner";
            IEnumerable<ulong> result = from r in user.Guild.Roles
                                        where r.Name == targetRoleName
                                        select r.Id;
            ulong roleId = result.FirstOrDefault();
            if (roleId == 0) return false;
            SocketRole targetRole = user.Guild.GetRole(roleId);
            return user.Roles.Contains(targetRole);
        }
    }
}
