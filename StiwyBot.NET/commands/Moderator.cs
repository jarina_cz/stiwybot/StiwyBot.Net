﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace StiwyBot.NET.commands
{
    public class Moderator : ModuleBase<SocketCommandContext>
    {
        [Command("purge", RunMode = RunMode.Async)]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task Purge([Remainder] string args = "2")
        {
            var messageCount = 1;
            var arg = args.Split(" ")[0];
            if (arg == "all")
            {
                messageCount = 30;
            }
            else
            {
                if (!string.IsNullOrEmpty(arg))
                {
                    try
                    {
                        messageCount = int.Parse(arg);
                    }
                    catch (FormatException)
                    {
                        await Context.Channel.SendMessageAsync("Usage: 'purge <number|all>");
                        return;
                    }
                }
            }
            var messages = await Context.Channel.GetMessagesAsync(messageCount).FlattenAsync();
            if (messages != null)
            {
                foreach (var message in messages)
                {
                    await Context.Channel.DeleteMessageAsync(message);
                }

            }
        }
    }
}
