﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

namespace StiwyBot.NET
{
    public class MessageHandler
    {
        private DiscordSocketClient _client;
        private CommandService _service;
        public MessageHandler(DiscordSocketClient client)
        {
            _client = client;
            _service = new CommandService();
            _service.AddModulesAsync(Assembly.GetEntryAssembly(), null);
            _client.MessageReceived += HandleCommandAsync;
            _client.MessageReceived += ProfanityFilter;
        }

        private async Task HandleCommandAsync(SocketMessage s)
        {
            var msg = s as SocketUserMessage;
            if (msg == null || msg.Author.IsBot) return;

            var context = new SocketCommandContext(_client, msg);

            var argPos = 0;
            if (msg.HasStringPrefix(Config.Bot.CmdPrefix, ref argPos)
              || msg.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var result = await _service.ExecuteAsync(context, argPos, null);
                if (!result.IsSuccess && result.Error != CommandError.UnknownCommand)
                {
                    await context.Channel.SendMessageAsync(result.ErrorReason);
                }
            }
        }

        private async Task ProfanityFilter(SocketMessage s)
        {
            var msg = s as SocketUserMessage;
            if (msg == null || msg.Author.IsBot || !(msg.Channel is SocketTextChannel)) return;

            var argPos = 0;
            var msgContent = msg.Content;
            if (msg.HasStringPrefix(Config.Bot.CmdPrefix, ref argPos)
              || msg.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                msgContent = msgContent.Substring(argPos);
            }
            if (Config.Bot.ProfanityFilter)
            {
                Regex regex = new Regex(Config.Bot.ProfanityFilterString, RegexOptions.IgnoreCase);
                if (regex.IsMatch(msgContent))
                {
                    await msg.DeleteAsync();
                    Console.WriteLine($"Edited message from { msg.Author.Username} for using mature language");
                    await msg.Channel.SendMessageAsync($"Last message from {msg.Author.Username} removed by StiwyBot for using mature language");
                }
            }
            await Task.CompletedTask;
        }
    }
}
